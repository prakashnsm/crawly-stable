### Questo PROGETTO E' IN  FREEZE

Con la versione 1.0 di Cralwy  e 6 mesi di sviluppo abbiamo esaurito il progetto richiestoci dal Corso di Metodologie di programmazione
Dopo la consegna non ha più senso sviluppare un progetto privato.
Questo repository diventerà pubblico il giorno 26 Gennaio 2016
Questo repository rimarrà freezato in questo stato, come prima versione STABILE(e per avere una traccia dei cambiamenti fatti nel tempo).
Lo sviluppo continuerà su github

GitHub Link = https://github.com/llde/crawly

### Nel frattempo
Provate il programma... Se riscontrate dei problemi  segnalate dei bug report qui ()per la versione stabile) o su github (per la development).
Se volte contribuire andate su github
Features request vanno fatte su github.

##Controllare il file TODO
Per verificare i piani di sviluppo. Il TODO quì presente non sarà più aggiornato dopo il 26 Gennaio 2016
Fasre riferimento al TODO su github

### CHE IL CIAVARELLA SIA CON VOI.

Crawly Team
Lorenzo llde Ferrillo  -dev

Manolo handymanny Lelli  -dev, grafico

Gianmarco Caruso - minor bugs.


Disclaimer:

Cralwy è rilasciatodocumentazione sotto licenza GPLv3.

Si chiede gentilmente di lasciare una notifica ad una delle nostre inbox su github o bitbucket in caso di sviluppo di tale modifica
O su github e bitbucket fare un Fork dell'applicazione. (le notifiche arrivano di default)

Gli asset non programmatici (non file .java, .scala, .fxml, .class) sono da considerarsi rialsciati sotto licenza
CreativeCommons ShareAlike, NonCommercial Use Only, o se sono risorse prese da internet come pubblico dominio.


Know Issues:
1) L'algoritmo di distanza tra coppie di uri non riporta il risultato corretto.
2) JavaFX Webengine consuma un fottio di memoria  @Message to Oracle. Ave Sithis